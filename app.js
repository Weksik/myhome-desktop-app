const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const { mongoURI } = require("./env.js");
const shutdownRoute = require("./routes/pcremote/pcremote");
const systemRoute = require("./routes/system/system")

const backend = express();

mongoose
  .connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log(
      "\x1b[33m%s\x1b[0m",
      ` |    MongoDB has been connected!    |
 =====================================`
    );
  })
  .catch((error) => {
    console.log(error);
  });

backend.use(bodyParser.urlencoded({ extended: true }));
backend.use(bodyParser.json());
backend.use(require("cors")());

backend.use("/api/pcremote", shutdownRoute);
backend.use("/api/system", systemRoute);


module.exports = backend;
