const exec = require("child_process").exec;
const Log = require("../models/Log");
const moment = require("moment");
const env = require("../env");

let remainingTime = null;
let stateMachine = "on";

const createLogDB = (action, status, timer) => {
  return new Log({
    user: env.username,
    time: new Date(),
    action: action,
    status: status,
    timer: timer,
  });
};

module.exports.getLast = async (req, res) => {
  try {
    const logs = await Log.find().sort("-created_at").limit(1);
    res.status(200).json(logs);
  } catch (e) {
    console.log(e);
  }
};

module.exports.getAll = async (req, res) => {
  try {
    const logs = await Log.find({});

    res.status(200).json(logs);
  } catch (e) {
    console.log(e);
  }
};

module.exports.shutdown = async (req, res) => {
  const log = createLogDB("shutdown", "off");
  function shutdown(callback) {
    exec("shutdown -s", function (error, stdout, stderr) {
      callback(stdout);
    });
  }

  try {
    if (stateMachine === "on") {
      await log.save();
      res.status(201).json(log);
      stateMachine = "off";
      shutdown(function (output) {
        console.log(output);
      });
    } else {
      res.status(404).json({ message: "PC is already shutdown!" });
      throw new Error("PC is already shutdown!");
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports.cancel = async (req, res) => {
  function shutdown(callback) {
    exec("shutdown -a", function (error, stdout, stderr) {
      callback(stdout);
    });
  }

  const log = createLogDB("cancel", "on");

  try {
    shutdown(function (output) {
      console.log(output);
    });
    await log.save();
    remainingTime = null;
    stateMachine = "on";

    res.status(200).json(log);
  } catch (e) {
    res.status(404).json({
      message: `Error : ${e}`,
    });
  }
};

module.exports.timer = async (req, res) => {
  const { timer } = req.body;

  const log = createLogDB("timer", "on", timer * 60);

  remainingTime = moment().add(timer, "minutes");
  if (timer) {
    try {
      await log.save();
      exec(`shutdown -s -t ${timer * 60}`);
      stateMachine = "timer";
      res.status(201).json(log);
    } catch (e) {
      console.log("Error");
      res.status(400).json({
        message: "Something went wrong",
      });
      return;
    }
  }
};

module.exports.remainingTime = async (req, res) => {
  if (remainingTime) {
    res.status(200).json({
      message: moment(remainingTime) - moment(),
    });
  } else {
    res.status(404).json({
      message: "Timer is not exist!",
    });
  }
};
