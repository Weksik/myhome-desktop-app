const env = require("../env")

module.exports.getInfo = async (req, res) => {
  try {
    res.status(200).json(env);
  } catch (e) {
    console.log(e);
  }
};