const os = require("os");

module.exports = {
  PORT: 54321,
  API_URL: os.networkInterfaces().Ethernet.find(interface => interface.family === "IPv4").address,
  mongoURI:
    "mongodb+srv://Admin:JU2DJHpa8je5@cluster0.x87el.mongodb.net/<dbname>?retryWrites=true&w=majority",
  hostname: os.hostname(),
  platform: os.platform(),
  username: os.userInfo().username,
};
