const { app, BrowserWindow } = require("electron");
const path = require("path");
const url = require("url");
const { API_URL, PORT } = require("./env.js");
const backend = require("./app");

let win;

backend.listen(PORT, API_URL, () =>
  console.log(`Server has been started: ${API_URL}:${PORT}`)
);

const createWindiw = () => {
  //Create application with params
  win = new BrowserWindow({
    width: 600,
    height: 600,
    minWidth: 300,
    minHeight: 300,
    maxWidth: 600,
    icon: __dirname + "/img/icon.png",
  });

  //Show where is our index html and protocol
  win.loadURL(
    url.format({
      pathname: path.join(__dirname, "index.html"),
      protocol: "file:",
      slashes: true,
    })
  );

  //Open chrome devtools
  // win.webContents.openDevTools();

  //When app closed
  win.on("closed", () => {
    win = null;
  });
};

//Start application
app.on("ready", createWindiw);
