const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const logSchema = new Schema({
  user: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    default: new Date(),
  },
  action: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  timer: {
    type: String,
  }
});

module.exports = mongoose.model("log", logSchema);