const express = require("express");
const controller = require("../../controllers/shutdown");
const router = express.Router();

router.get("/", controller.getAll);
router.get("/last", controller.getLast);
router.get("/shutdown", controller.shutdown);
router.get("/cancel", controller.cancel);
router.get("/remainingTime", controller.remainingTime);
router.post("/timer", controller.timer);


module.exports = router;
