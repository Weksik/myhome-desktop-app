const express = require("express");
const controller = require("../../controllers/system.js");
const router = express.Router();

router.get("/", controller.getInfo);

module.exports = router;
